package com.example.myproject;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class SignInActivity extends AppCompatActivity {

    private EditText password;
    private EditText email;
    private EditText name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in_activity);

        email = findViewById(R.id.email);
        name = findViewById(R.id.name);
        password = findViewById(R.id.password);

        findViewById(R.id.signIn).setOnClickListener(v ->
                Firebase.auth
                        .signInWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                        .addOnCompleteListener(command -> {
                            if (command.isSuccessful()) {
                                startActivity(new Intent(
                                        SignInActivity.this,
                                        MainActivity2.class));
                                finish();
                            } else {
                                new MaterialAlertDialogBuilder(this)
                                        .setTitle("Error")
                                        .setMessage(command.getException().getLocalizedMessage())
                                        .setPositiveButton("OK", null)
                                        .create()
                                        .show();
                            }
                        })
        );

        findViewById(R.id.toSignUp).setOnClickListener(v ->
                startActivity(
                        new Intent(
                                SignInActivity.this,
                                SignUpActivity.class
                        )
                )
        );
    }
}