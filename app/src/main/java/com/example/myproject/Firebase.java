package com.example.myproject;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

public class Firebase {

    public static FirebaseAuth auth = FirebaseAuth.getInstance();
    public static FirebaseFirestore firestore = FirebaseFirestore.getInstance();
}
