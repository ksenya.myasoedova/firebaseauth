package com.example.myproject;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class SignUpActivity extends AppCompatActivity {

    private EditText password;
    private EditText email;
    private EditText name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        email = findViewById(R.id.emailSignUp);
        password = findViewById(R.id.passwordSignUp);
        name = findViewById(R.id.nameSignUp);

        findViewById(R.id.signUp).setOnClickListener(v ->
                Firebase.auth
                        .createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                        .addOnCompleteListener(command -> {
                            if (command.isSuccessful()) {
                                Firebase.firestore
                                        .collection("users")
                                        .document(Firebase.auth.getCurrentUser().getUid())
                                        .set(new User(name.getText().toString()))
                                        .addOnCompleteListener(task -> {
                                            if (task.isSuccessful()) {
                                                startActivity(
                                                        new Intent(
                                                                SignUpActivity.this,
                                                                MainActivity2.class
                                                        )
                                                );

                                                finish();
                                            } else
                                                showError(task.getException().getLocalizedMessage());
                                        });
                            } else
                                showError(command.getException().getLocalizedMessage());
                        })
        );

        findViewById(R.id.toSignIn).setOnClickListener(v ->
                startActivity(
                        new Intent(
                                SignUpActivity.this,
                                SignInActivity.class
                        )
                )
        );

        findViewById(R.id.toSignIn).setOnClickListener(v -> finish());
    }

    private void showError(String message) {
        new MaterialAlertDialogBuilder(this)
                .setTitle("Error")
                .setMessage(message)
                .setPositiveButton("OK", null)
                .create()
                .show();
    }
}