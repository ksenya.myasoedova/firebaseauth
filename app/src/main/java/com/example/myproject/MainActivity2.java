package com.example.myproject;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Firebase.firestore
                .collection("users")
                .document(Firebase.auth.getCurrentUser().getUid())
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        User user = task.getResult().toObject(User.class);

                        ((TextView) findViewById(R.id.name)).setText(user.name);
                        findViewById(R.id.progressBar).setVisibility(View.GONE);
                    } else
                        new MaterialAlertDialogBuilder(this)
                                .setTitle("Error")
                                .setMessage(task.getException().getLocalizedMessage())
                                .setPositiveButton("OK", null)
                                .create()
                                .show();
                });
    }
}